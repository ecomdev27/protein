$(document).ready(function() {
  initSlider();
})

const initSlider = function() {
  $("[data-slider]").each(function() {
    var $slider = $(this);
    if($slider.attr('data-mobile') == 'true' && $(window).width() > 749) {
    }
    else {
      $slider.slick({
        slidesToShow: 2,
        arrows: false,
        dots: true
      });
    }
  })
}